using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Apps.Http;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.GoogleTranslate;

class MyAppsConfigGoogleApiKeyStore : IArgStore<string>
{
    private readonly Config settings;

    public MyAppsConfigGoogleApiKeyStore(Config settings)
    {
        this.settings = settings;
    }

    public void SetArg(string arg)
    {
        settings.GoogleApiKey = arg;
    }

    public bool TryGetArg(out string arg)
    {
        arg = settings.GoogleApiKey;
        return !string.IsNullOrEmpty(arg);
    }
}
