using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Apps.Http;
using QueryStorm.Apps.Services;

namespace Windy.GoogleTranslate;

public class TranslateFunction
{
    private readonly ISyncRunner syncRunner;
    private readonly ILicensingService licensingService;
    private readonly HttpRequestManager httpRequestManager;

    public TranslateFunction(Config config, ISyncRunner syncRunner, ILicensingService licensingService, IDialogServiceExt dialogService, IQueryStormLogger logger)
    {
        this.syncRunner = syncRunner;
        this.licensingService = licensingService;

        this.httpRequestManager = new GoogleHttpRequestManager(
            dialogService, "ApiKeyDialog",
            new MyAppsConfigGoogleApiKeyStore(config),
            20, logger);
    }

    [ExcelFunction(Name = "Windy.GoogleDetectLanguage"), Cached]
    public async Task<string[,]> DetectLanguage(string[,] texts)
    {
        EnsureAccess(1, nameof(DetectLanguage));

        var resultStr = await httpRequestManager.SendAsync(apiKey =>
        {
            var url = $"https://translation.googleapis.com/language/translate/v2/detect";
            var data = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>("key", apiKey) };
            foreach (var s in texts)
                data.Add(new KeyValuePair<string, string>("q", s));
            var content = new FormUrlEncodedContent(data);
            return new HttpRequestMessage(HttpMethod.Post, url) { Content = content };
        });

        var result = JsonConvert.DeserializeObject<DetectionResult>(resultStr);
        var detectedLangsArray = result.Data.Detections.Select(d => d[0].Language).ToArray();
        var cols = texts.GetLength(1);
        for (int i = 0; i < detectedLangsArray.Length; i++)
        {
            var rowIdx = i / cols;
            var colIdx = i % cols;
            texts[rowIdx, colIdx] = detectedLangsArray[i];
        }
        return texts;
    }

    [ExcelFunction(Name = "Windy.GoogleTranslate"), Cached]
    public async Task<string[,]> Translate(string[,] texts, string targetLanguage, [ExcelArgument("Skip or pass null to auto-detect source language")] string sourceLanguage = null)
    {
        EnsureAccess(2, nameof(Translate));

        if (string.Equals(sourceLanguage, targetLanguage, StringComparison.OrdinalIgnoreCase))
            return texts;

        var resultStr = await httpRequestManager.SendAsync(apiKey =>
        {
            var url = $"https://translation.googleapis.com/language/translate/v2";

            var data = new List<KeyValuePair<string, string>>()
	        {
	            new KeyValuePair<string, string>("target", targetLanguage),
	            new KeyValuePair<string, string>("source", sourceLanguage),
	            new KeyValuePair<string, string>("format", "text"),
	            new KeyValuePair<string, string>("key", apiKey),
	        };

            foreach (var s in texts)
            {
                data.Add(new KeyValuePair<string, string>("q", s));
            }

            var content = new FormUrlEncodedContent(data);
            return new HttpRequestMessage(HttpMethod.Post, url) { Content = content };
        });

        var result = JsonConvert.DeserializeObject<TranslateResult>(resultStr);
        var results = result.Data.Translations.Select(t => t.TranslatedText).ToArray();

        var cols = texts.GetLength(1);
        for (int i = 0; i < results.Length; i++)
        {
            var rowIdx = i / cols;
            var colIdx = i % cols;
            texts[rowIdx, colIdx] = results[i];
        }
        return texts;
    }

    private void EnsureAccess(byte featureId, string featureName)
    {
        licensingService.EnsureAccess(featureId, featureName);
    }
}

public class Data
{
    public List<Translation> Translations { get; set; }
}

public class TranslateResult
{
    public Data Data { get; set; }
}

public class Translation
{
    public string TranslatedText { get; set; }
    public string DetectedSourceLanguage { get; set; }
}

public class DetectionInfo
{
    public bool IsReliable { get; set; }
    public string Language { get; set; }
    public double Confidence { get; set; }
}

public class DetectionData
{
    public List<List<DetectionInfo>> Detections { get; set; }
}

public class DetectionResult
{
    public DetectionData Data { get; set; }
}

